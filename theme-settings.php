<?php

/**
 * @file
 * theme-settings.php
 *
 * Provides theme settings.
 *
 * @see ./includes/settings.inc
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function searchify_form_system_theme_settings_alter(&$form, FormStateInterface $form_state, $form_id = NULL) {
  $base_theme_path = drupal_get_path('theme', 'mbase');
  if (!$base_theme_path) {
    $form = array();
    $form['nombase'] = array(
      '#type' => 'markup',
      '#title' => t('Error'),
      '#markup' => '<div class = "nombasetheme">This theme depend on
        <strong><a href = "https://www.drupal.org/project/mbase" target = "_blank">mbase</a></strong>
        base theme. Please install <strong><a href = "https://www.drupal.org/project/mbase" target = "_blank">
        mbase</a></strong> theme first. Please read the readme.txt file.</div>',
    );
    unset($form['color_scheme_form']);
  }
  else {
    include_once $base_theme_path . '/includes/helper.inc';

    $form['searchifygroup'] = array(
      '#type' => 'details',
      '#title' => t('Searchify Settings'),
      '#group' => 'mbase',
    );
    $default_search_style = _mbase_setting('search_style', 'searchify', NULL);
    $default_search_style = $default_search_style ? $default_search_style : 'default';
    $form['searchifygroup']['search_style'] = array(
      '#type' => 'select',
      '#title' => t('Select the Search style'),
      '#options' => _searchify_get_styles(),
      '#default_value' => $default_search_style,
      '#description' => t('Select the theme style.'),
    );
    $default_bg_image = _mbase_setting('search_bg_image', 'searchify', NULL);
    global $base_path;
    $theme_default_bg_image = $base_path . drupal_get_path('theme', 'searchify') . '/styles/bing/bg.jpg';
    $form['searchifygroup']['search_bg_image'] = array(
      '#type' => 'textfield',
      '#title' => t('Background Image'),
      '#default_value' => trim($default_bg_image) ? $default_bg_image : $theme_default_bg_image,
      '#description' => t('Paste the Image URL (full or relative) for bing style home page.'),
    );
    $default_search_button_text = _mbase_setting('search_button_text', 'searchify', NULL);
    $default_search_button_text = $default_search_button_text ? $default_search_button_text : 'Discover Now';
    $form['searchifygroup']['search_button_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Search Button text'),
      '#default_value' => $default_search_button_text,
      '#description' => t('Enter the search button text.'),
    );
    $default_home_above_searchbox = _mbase_setting('home_above_searchbox', 'searchify', NULL);
    $form['searchifygroup']['home_above_searchbox'] = array(
      '#type' => 'textarea',
      '#title' => t('Home page Above Searchbox'),
      '#description' => t('Enter the markup to display above the home page search box.'),
      '#default_value' => $default_home_above_searchbox ? $default_home_above_searchbox : 'Search from 1000s of contents.',
    );
  }
}
