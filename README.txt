Requirements:
After setup your drupal 8 site. Download and enable(Don't set as default) this
base theme https://www.drupal.org/project/mbase.
Enable the search module and give permissions to anonymous users to use search.

After Install Searchify, Change the settings in theme settings page.
There are 3 Styles:
 1). Google : Tried to imitate the google
 2). Bing : Tried to imitate the bing.com
 3). Searchify : Simple drupal search theme.
Make sure you Use "google" color for "google style" and "bing" color for
"bing style". For Default style, you can use any color combo for this.
